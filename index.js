let http = require('http');
const port = 4000;

http.createServer((request, response) => {


	if(request.url && request.method === 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write('Welcome to the booking system.');
		response.end();
	}

	else if(request.url === '/profile' && request.method === 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write('Welcome to your profile.');
		response.end();
	}

	else if(request.url === '/courses' && request.method === 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write("Here's our courses available.");
		response.end();
	}

	else if(request.url === '/addcourse' && request.method === 'POST'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write('Add a course to our resources');
		response.end();
	}

	else if(request.url === '/updatecourse' && request.method === 'PUT'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write('Update a course to our resources');
		response.end();
	}

	else if(request.url === '/archivecourses' && request.method === 'DELETE'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write('Archive course to our resources');
		response.end();
	}

	else{
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.write('Request cannot be completed');
		response.end();
	}

}).listen(port);

console.log('Hello')